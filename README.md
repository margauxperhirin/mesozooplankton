# Mesozooplankton

Raw data and R scripts used for the following study : "Impact of mesozooplankton diversity on the ecosystem functioning in the Sargasso Sea, using molecular and imaging data"

## How does it work ?

In this GitLab, you can find :
* Scripts : all the R scripts needed to realise the analyses 
* Functions : 2 functions created for this study and used in some scripts
* Data : all the raw data files needed for the scripts to be run, other files will be produced during the analysis

The analysis should follow in the following path :
1. formatting_imaging.R
    - Input : 
        - ecotaxa_export.tsv
    - Outputs :
        - community_500um.csv       (descriptors data for all the 69,950 images)
        - dw_cruises.csv            (dry weight computed from images per Day/Night samplings)
2. formmatting_metada.R
    - Input : 
        - env_data_1000m.csv
        - export_raw.csv
        - dw_cruises.csv
        - zoopk_biomass.csv
    - Outputs :
        - metadata_sampling.csv     (environmental, export and biomass data per Day/Night samplings)
3. formmatting_molecular.R
    - Input : 
        - taxonomy_04-05.txt
        - ASV_abundances.txt
        - ASV_sequences.txt
    - Outputs :
        - ASV_rarefy.csv            (Number of reads per sampling, taxonomy and fasta sequences for each ASV)
        - ASV_0-05.csv              (Number of reads per sampling for the top 225 ASVs in terms of relative abundances)
4. img_morphospace.R
    - Input : 
        - metadata_sampling.csv
        - community_500um.csv
        - extract_and_show.R
    - Outputs :
        - img_samplingcoord.csv     (coordinates along the 4 first axes of the morphospace for each Day/Night sampling point)
5. mol_pcoa_clustering.R
    - Input : 
        - metadata_sampling.csv
        - zoo_biom_cruise_DN.csv
    - Outputs :
        - clusters_pcoa.csv         (matching PCoA cluster for each Day/Night sampling)
6. mol_indval.R
    - Input : 
        - ASV_0-05.csv
        - ASV_rarefy.csv
        - clusters_pcoa.csv
    - Outputs :
        - ASV_IndVal.csv            (225 ASVs and their eventual specific cluster, IndVal index with corresponding p-value and relative abundance in the concern cluster)
7. mol_rda.R
    - Input : 
        - metadata_sampling.csv
        - ASV_0-05.csv
        - clusters_pcoa.csv
8. mol_network.R
    - Input : 
        - ASV_0-05.csv
        - ASV_rarefy.csv
        - metadata_sampling.csv
        - clusters_pcoa.csv
        - img_samplingcoord.csv
        - ASV_IndVal.csv
        - network_and_save_relative.R
    - Outputs :
        - ASV_IndVal_Modules.csv    (same than ASV_Indval.csv but with taxonomy, Fasta sequence and eventual module belonging for each ASV)
        - mod_length.csv            (length of each identified module)
        - mol_toponetwork.csv       (network topological features)
9. pisces_output.R
    - Input : 
        - BATS_output.nc
        - export_raw.csv
        - zoo_biom_cruise_DN.csv


